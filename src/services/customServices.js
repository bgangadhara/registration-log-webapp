import IdentityService from './identityService';
import UserInfoService from './userInfoService';
import RegistrationLogService from './registrationLogService';
import PartnerSaleRegistrationService from './partnerSaleRegistrationService';
import ConsumerSaleRegistrationService from './consumerSaleRegistrationService';
import AccountService from './accountService';
import PartnerRepService from './partnerRepService';
import ClaimSpiffService from './claimSpiffService';
import ExtendedWarrantyService from './extendedWarrantyService';
import CountryAndRegionService from './countryAndRegionService';
import SpiffMasterDataService from './spiffMasterDataService';

angular
    .module("registrationLogWebApp.services",[])
    .service("identityService", IdentityService)
    .service("userInfoService", UserInfoService)
    .service("registrationLogService", RegistrationLogService)
    .service("partnerSaleRegistrationService", PartnerSaleRegistrationService)
    .service("consumerSaleRegistrationService", ConsumerSaleRegistrationService)
    .service("accountService", AccountService)
    .service("partnerRepService", PartnerRepService)
    .service("claimSpiffService", ClaimSpiffService)
    .service("extendedWarrantyService", ExtendedWarrantyService)
    .service("countryAndRegionService", CountryAndRegionService)
    .service("spiffMasterDataService", SpiffMasterDataService);


