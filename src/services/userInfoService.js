import IdentityServiceSdk from 'identity-service-sdk';

/**
 * @class {UserInfoService}
 */

export default class UserInfoService {

    _$q;

    _identityServiceSdk:IdentityServiceSdk;

    constructor(
        $q,
        identityServiceSdk : IdentityServiceSdk
    ){
        this.getUserInfo = function(accessToken){
            return $q(resolve =>
                identityServiceSdk
                    .getUserInfo(accessToken)
                    .then(
                        userInfo => resolve(userInfo)
                    ).catch(function(error){
                        console.log("error in UserInfoService......", error);
                    })
            )
        }
    }
}

UserInfoService.$inject = [
    '$q',
    'identityServiceSdk'
];