import ConsumerSaleRegistrationServiceSdk from 'consumer-sale-registration-service-sdk';
import Iso3166Sdk from 'iso-3166-sdk';

export default class ConsumerSaleRegistrationService {

    _$q;
    _consumerSaleRegistrationServiceSdk : ConsumerSaleRegistrationServiceSdk;
    
    _iso3166Sdk : Iso3166Sdk;

    constructor(
        $q,
        consumerSaleRegistrationServiceSdk : ConsumerSaleRegistrationServiceSdk,
        iso3166Sdk : Iso3166Sdk
    ){        
        this.getconsumerSaleRegistrationInfo = function( draftId, accessToken ){
            return $q( resolve =>
                consumerSaleRegistrationServiceSdk
                .getConsumerSaleRegDraftWithId( draftId, accessToken )
                    .then(
                        consumerSaleRegistrationInfo =>
                            resolve(consumerSaleRegistrationInfo)
                    ).catch(function(error){
                        console.log("error in consumerSaleRegistrationInfo......", error);
                    })
            )
        }
        
        this.getCountryInfo = function(){
            return $q(
                resolve =>
                    iso3166Sdk
                        .listCountries()
                        .then(
                            countries =>
                                resolve(countries)
                        )
            )
        }
        
        this.getRegionInfo = function( countryCode ){
            return $q(resolve =>
                iso3166Sdk
                    .listSubdivisionsWithAlpha2CountryCode(countryCode)
                    .then(
                        regions =>
                            resolve(regions)
                    )
            )
        }
    }
}

ConsumerSaleRegistrationService.$inject = [
    '$q',
    'consumerSaleRegistrationServiceSdk',
    'iso3166Sdk'
];