export default class RegistrationInfoCtrl {

    constructor(
        $scope,
        $q,
        identityService,
        partnerSaleRegistrationService,
        consumerSaleRegistrationService,
        accountService,
        partnerRepService
    ){        
        this.loader = true;
        
        this.draftId = localStorage.getItem('draftId');
        this.installDate = localStorage.getItem('installDate');
        this.productInfo = {};
        
        var self = this;
        
        identityService.getAccessToken()
            .then(accessToken => {
                this.accessToken = accessToken;
            
                if( self.installDate !== "null" ){
                    self.flag = "commercial";
                    partnerSaleRegistrationService.getPartnerSaleRegistrationInfo( this.draftId , this.accessToken )
                    .then(partnerSaleRegistrationInfo => {
                            this.partnerSaleInfo = partnerSaleRegistrationInfo;
                            this.productInfo.simpleLineItems = this.partnerSaleInfo.simpleLineItems;
                            this.productInfo.compositeLineItems = this.partnerSaleInfo.compositeLineItems;
                            this.getCommercialInfo();
                        }
                    )
                } else {
                    self.flag = "consumer";
                    consumerSaleRegistrationService.getconsumerSaleRegistrationInfo( this.draftId , this.accessToken )
                    .then(consumerSaleRegistrationInfo => {
                            this.consumerSaleRegistrationInfo = consumerSaleRegistrationInfo;                            
                            this.productInfo.simpleLineItems = this.consumerSaleRegistrationInfo.simpleLineItems;
                            this.productInfo.compositeLineItems = this.consumerSaleRegistrationInfo.compositeLineItems;
                            this.getConsumerInfo();
                        }
                    )
                }
            });
        
        this.getCommercialInfo = function(){
            var self = this;
            
            var promise1 = accountService.getAccountInfo(this.partnerSaleInfo.facilityId, this.accessToken)
                .then(accountInfo => { 
                        this.partnerAccountInfo = accountInfo;                    
                        this.partnerAccountInfo._customerBrand = this.partnerAccountInfo._customerBrand ? this.partnerAccountInfo._customerBrand: "No Match";
                        this.partnerAccountInfo._customerSubBrand = this.partnerAccountInfo._customerSubBrand ? this.partnerAccountInfo._customerSubBrand: "No Match";
                    }
                )
            
            var promise2 = partnerSaleRegistrationService.getPartnerContactInfo(this.partnerSaleInfo.facilityId, this.accessToken)
                .then(contactInfo => { 
                        this.partnercontactInfo = contactInfo[0];
                    }
                )
            
            var promise3 = partnerSaleRegistrationService.getCustomerSourceInfo( this.accessToken )
                .then(customerSources => { 
                        angular.forEach(customerSources , function( val , key ){
                            if(val.id == self.partnerSaleInfo.customerSourceId){
                                self.partnerSaleInfo.customerSource = val._name;
                            }
                        });
                    }
                )
            
            var promise4 = partnerSaleRegistrationService.getManagementCompanyInfo( this.accessToken )
                .then(managementCompanies => { 
                        var managementCompanies = JSON.parse(managementCompanies.response);
                        angular.forEach(managementCompanies ,function( val , key ){
                            if(val.id == self.partnerSaleInfo.managementCompanyId){
                                self.partnerSaleInfo.managementCompany = val.name;
                            }
                        });
                    }
                )
            
            var promise5 = partnerSaleRegistrationService.getBuyingGroupInfo( this.accessToken )
                .then(buyingGroups => { 
                        var buyingGroupList = JSON.parse(buyingGroups.response);
                        angular.forEach( buyingGroupList , function( val , key ){
                            if(val.id == self.partnerSaleInfo.buyingGroupId){
                                self.partnerSaleInfo.buyingGroup = val.name;
                            }
                        });
                    }
                )
            
            var promisesArray = [ promise1, promise2, promise3, promise4, promise5 ];
            
            if(this.partnerSaleInfo.partnerRepUserId){
                var promise6 = partnerRepService.getDealerRep(this.partnerSaleInfo.partnerRepUserId, this.accessToken )
                .then(dealerRep => { 
                        self.partnerSaleInfo.dealerRep = dealerRep.firstName + " " + dealerRep.lastName;
                    }
                )
                promisesArray.push( promise6 );
            }
            
            $q.all( promisesArray ).then(function(value) {
                self.loader = false;
            }, function(reason) {                
                self.loader = true;
            });
        }
        
        this.getConsumerInfo = function(){
            //splitting address - 1 and address - 2
            //self.tempStorage = self.consumerSaleRegistrationInfo.address.street.split('!?!');
            self.consumerSaleRegistrationInfo.address1 = self.consumerSaleRegistrationInfo.address.addressLine1;
            self.consumerSaleRegistrationInfo.address2 = self.consumerSaleRegistrationInfo.address.addressLine2;
            
            var promise1 = consumerSaleRegistrationService.getCountryInfo()
                .then(
                    countries => {
                        angular.forEach( countries , function( val , key ){
                            if(val.alpha2Code == self.consumerSaleRegistrationInfo.address.countryIso31661Alpha2Code){
                                self.consumerSaleRegistrationInfo.country = val.name;
                            }
                        })
                    }
                );
            
            var promise2 = consumerSaleRegistrationService.getRegionInfo( self.consumerSaleRegistrationInfo.address.countryIso31661Alpha2Code )
                .then(
                    regions => {
                        angular.forEach( regions , function( val , key ){
                            if(val.code == self.consumerSaleRegistrationInfo.address.regionIso31662Code){
                                self.consumerSaleRegistrationInfo.region = val.name;
                            }
                        })
                    }
                );
            var promisesArray = [ promise1, promise2 ];
            
            if(this.consumerSaleRegistrationInfo.partnerRepUserId){            
                var promise3 = partnerRepService.getDealerRep(this.consumerSaleRegistrationInfo.partnerRepUserId, this.accessToken )
                .then(dealerRep => { 
                        self.consumerSaleRegistrationInfo.dealerRep = dealerRep.firstName + " " + dealerRep.lastName;
                    }
                )
                promisesArray.push( promise3 );
            }
            
            $q.all( promisesArray ).then(function(value) {
                self.loader = false;
            }, function(reason) {                
                self.loader = true;
            });
        }
    }
}

RegistrationInfoCtrl.$inject = [
    '$scope',
    '$q',
    'identityService',
    'partnerSaleRegistrationService',
    'consumerSaleRegistrationService',
    'accountService',
    'partnerRepService'
];
