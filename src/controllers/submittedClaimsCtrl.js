export default class SubmittedClaimsCtrl {
    
    _$scope;
    
    _identityService;
    
    _claimSpiffService;
    
    _partnerSaleRegistrationService;
    
    _consumerSaleRegistrationService;
    
    _partnerRepService;
    
    _prepareClaimSpiffRequestFactory;
    
    _spiffMasterDataService;
    
    _mergeSpiffDataWithAssetsFactory;

    constructor(
        $scope,
        identityService,
        claimSpiffService,
        partnerSaleRegistrationService,
        consumerSaleRegistrationService,
        partnerRepService,
        prepareClaimSpiffRequestFactory,
        spiffMasterDataService,
        mergeSpiffDataWithAssetsFactory
    ){  
        if(!$scope){
            throw new TypeError("$scope required");
        }
        this._$scope = $scope;
        
        if(!identityService){
            throw new TypeError("identityService required");
        }
        this._identityService = identityService;
        
        if(!claimSpiffService){
            throw new TypeError("claimSpiffService required");
        }
        this._claimSpiffService = claimSpiffService;
        
        if(!partnerSaleRegistrationService){
            throw new TypeError("partnerSaleRegistrationService required");
        }
        this._partnerSaleRegistrationService = partnerSaleRegistrationService;
        
        if(!consumerSaleRegistrationService){
            throw new TypeError("consumerSaleRegistrationService required");
        }
        this._consumerSaleRegistrationService = consumerSaleRegistrationService;
        
        if(!partnerRepService){
            throw new TypeError("partnerRepService required");
        }
        this._partnerRepService = partnerRepService;
        
        if(!prepareClaimSpiffRequestFactory){
            throw new TypeError("prepareClaimSpiffRequestFactory required");
        }
        this._prepareClaimSpiffRequestFactory = prepareClaimSpiffRequestFactory;
        
        if(!spiffMasterDataService){
            throw new TypeError("spiffMasterDataService required");
        }
        this._spiffMasterDataService = spiffMasterDataService;
        
        if(!mergeSpiffDataWithAssetsFactory){
            throw new TypeError("mergeSpiffDataWithAssetsFactory required");
        }
        this._mergeSpiffDataWithAssetsFactory = mergeSpiffDataWithAssetsFactory;
        
        this.loader = true;        
        this.registrationId = localStorage.getItem('draftId');
        this.installDate = localStorage.getItem('installDate');
        var self = this;
        
        identityService.getAccessToken()
            .then(accessToken => {
                this.accessToken = accessToken;
            
                if( self.installDate !== "null" ){
                    self.flag = "commercial";
                    partnerSaleRegistrationService.getPartnerSaleRegistrationInfo( this.registrationId , this.accessToken )
                    .then(partnerSaleRegistrationInfo => {
                            this.claimInfo = partnerSaleRegistrationInfo;
                            this.getClaimInfo( this.claimInfo );
                        }
                    )
                } else {
                    self.flag = "consumer";
                    consumerSaleRegistrationService.getconsumerSaleRegistrationInfo( this.registrationId , this.accessToken )
                    .then(consumerSaleRegistrationInfo => {
                            this.claimInfo = consumerSaleRegistrationInfo;
                            this.getClaimInfo( this.claimInfo );
                        }
                    )
                }
            });
    }
    /**
     * methods
     */
    getClaimInfo( claimInfo ){
        var assets = {} , request;
        
        assets.simpleLineItems = claimInfo.simpleLineItems;
        assets.compositeLineItems = claimInfo.compositeLineItems;
        
        request = this._prepareClaimSpiffRequestFactory.prepareClaimSpiffRequest( assets );
        
        this._spiffMasterDataService
        .getSpiffAmountsWithSerialNumbers( request , this.accessToken)
        .then( response => {
                this.assetsList = 
                    this._mergeSpiffDataWithAssetsFactory
                    .mergeSpiffDataWithAssets( assets , response);
            
                this.calculateTotalSPIFFAmount( this.assetsList );
                
                this.getDealerInfo();
            }
        ).catch( 
            error => 
                console.log( error )
        )
    };
    
    getDealerInfo(){
        if(this.claimInfo.partnerRepUserId){
            this._partnerRepService
                .getDealerRep(this.claimInfo.partnerRepUserId, this.accessToken )
                .then(dealerRep => { 
                        this.claimInfo.dealerRep = dealerRep.firstName + " " + dealerRep.lastName;
                        this.loader = false;
                    }
                ).catch(
                    error => 
                        console.log( error )
                )
        } else {
            this.loader = false;
        }
    };
    
    calculateTotalSPIFFAmount( assetsList ){
        this.totalSpiffAmount = 0;
        
        angular.forEach(assetsList.simpleLineItems, (value, key) => {
            if( value.spiffAmount )
            this.totalSpiffAmount += value.spiffAmount;
        });
        
        angular.forEach(assetsList.compositeLineItems, (value, key) => {
            if( value.spiffAmount )
            this.totalSpiffAmount += value.spiffAmount;
        });
    };
}

SubmittedClaimsCtrl.$inject = [
    '$scope',
    'identityService',
    'claimSpiffService',
    'partnerSaleRegistrationService',
    'consumerSaleRegistrationService',
    'partnerRepService',
    'prepareClaimSpiffRequestFactory',
    'spiffMasterDataService',
    'mergeSpiffDataWithAssetsFactory'
];
