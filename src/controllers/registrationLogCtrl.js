export default class RegistrationLogCtrl {
    
    _$scope;
    
    _identityService;
    
    _userInfoService;
    
    _registrationLogService;

    constructor(
        $scope,
        identityService,
        userInfoService,
        registrationLogService
    ){  
        /**
         * initialization
         */
        if(!$scope){
            throw new TypeError('$scope required');
        }
        this._$scope = $scope;
        
        if(!identityService){
            throw new TypeError('identityService required');
        }
        this._identityService = identityService;
        
        if(!userInfoService){
            throw new TypeError('userInfoService required');
        }
        this._userInfoService = userInfoService;
        
        if(!registrationLogService){
            throw new TypeError('registrationLogService required');
        }
        this._registrationLogService = registrationLogService;
        
        this.loader = true;
        
        this.filterOptions = [
            {name: "0-90 Days", value: "0-90 Days"},
            {name: "This Year", value: "This Year"},
            {name: "Last Year", value: "Last Year"},
            {name: "Prior to Last Year", value: "Prior to Last Year"}
        ];        
        
        this.currentFilter = "0-90 Days";
        
        this.endDate = new Date();
        this.startDate = new Date().setDate(this.endDate.getDate() - 90);
        
        this.loadRegistrationLogs();
    }
    /**
     * methods
     */
    loadRegistrationLogs(){
        this._identityService.getAccessToken()
            .then(accessToken => {
                this._userInfoService.getUserInfo(accessToken)
                    .then(userInfo => {
                        this._registrationLogService.getRegistrationLogs(userInfo._account_id, accessToken )
                        .then(registrationLogs => {                                
                                this.loader = false;
                                this.registrationLogs = registrationLogs;
                            }
                        );
                    })
            }
        );
    };
    
    updateFilter( currentFilter ){
        var filterValue = currentFilter.toLowerCase();
        var today = new Date();
        switch(filterValue) {
            case "0-90 days":
                this.endDate = today;
                this.startDate = new Date().setDate( today.getDate() - 90 );
                break;
                
            case "this year":
                this.endDate = today;
                
                var thisYearStartDate = new Date().setMonth(0);
                thisYearStartDate = new Date( thisYearStartDate ).setDate(1);
                this.startDate = new Date( thisYearStartDate );
                break;
                
            case "last year":
                var lastYearEndDate = new Date().setFullYear( today.getFullYear() - 1 );
                lastYearEndDate = new Date( lastYearEndDate ).setMonth(11);
                lastYearEndDate = new Date( lastYearEndDate ).setDate(31);
                this.endDate = new Date( lastYearEndDate );
                
                var lastYearStartDate = new Date().setFullYear( today.getFullYear() - 1 );
                lastYearStartDate = new Date( lastYearStartDate ).setMonth(0);
                lastYearStartDate = new Date( lastYearStartDate ).setDate(1);
                this.startDate = new Date( lastYearStartDate );
                break;
                
            case "prior to last year":
                var alltherestEndDate = new Date().setFullYear( today.getFullYear() - 2 );
                alltherestEndDate = new Date( alltherestEndDate ).setMonth(11);
                alltherestEndDate = new Date( alltherestEndDate ).setDate(31);
                this.endDate = new Date( alltherestEndDate );
                
                this.startDate = new Date(1900,0,1);
                break;
            default:
                break;
        }                
    };

    showRegistrationDetails(draftId, installDate){
        localStorage.setItem( 'draftId' , draftId );
        localStorage.setItem( 'installDate' , installDate );
    };

    showClaimSpiff( draftId, installDate ){
        localStorage.setItem( 'draftId' , draftId );
        localStorage.setItem( 'installDate' , installDate );
    };

    showSubmittedEW( draftId ){
        localStorage.setItem( 'draftId' , draftId );
    };
}

RegistrationLogCtrl.$inject = [
    '$scope',
    'identityService',
    'userInfoService',    
    'registrationLogService'
];
